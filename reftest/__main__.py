#!/usr/bin/env python3
"""
First entry point.

Called by 'python reftest [options]' from the fft-in-tce-git-repo/reftest
directory.
"""

import sys
import reftest

def main():
	sys.exit(reftest.main())

if __name__ == "__main__":
	main()
